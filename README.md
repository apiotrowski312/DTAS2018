# DTAS2018

*  [Jak wygląda stateless component.](https://itnext.io/react-component-class-vs-stateless-component-e3797c7d23ab)
*  [Jak podłączyć reduxa do react-compoenent.](https://itnext.io/react-component-class-vs-stateless-component-e3797c7d23ab)
*  [Material-UI podstawy](https://medium.com/codingthesmartway-com-blog/getting-started-with-material-ui-for-react-material-design-for-react-364b2688b555)


Szybki tutorial GIT'a:
1.  Wchodzimy w nasz projekt [TUTAJ!](https://gitlab.com/apiotrowski312/DTAS2018)
2.  Wchodzimy w ISSIUE
3.  Wybieramy task który jest do nas przypisany
4.  Klikamy `Create merge request`
5.  Przechodzimy do konsoli na naszym komputerze
6.  Zaciągamy zmiany `git pull`
7.  Przełączamy się na branch z zadania z komendą `git checkout NUMERTASKA-TYTUŁ`
8.  Commity, pusher etc, chyba nie musze tłumaczyć?



## Przydatne paczki dla VS Code

https://gitlab.com/snippets/1769696