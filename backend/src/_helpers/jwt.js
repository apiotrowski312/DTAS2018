import Users from '../users/interface';
import config from '../config.js';
import expressJwt from 'express-jwt';

module.exports = jwt;

function jwt() {
  const secret = config.secret;

  return expressJwt({ secret, isRevoked }).unless({
    path: [
      // public routes that don't require authentication
      '/users/authenticate',
      '/users/register',
      new RegExp('uploads/.*'),
    ],
  });
}

async function isRevoked(req, payload, done) {
  const user = await Users.getById(payload.sub);

  // revoke token if user no longer exists
  if (!user) {
    return done(null, true);
  }

  done();
}
