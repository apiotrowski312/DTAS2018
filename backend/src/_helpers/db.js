import config from '../config.js';
import mongoose from 'mongoose';

mongoose.connect(
  config.connectionString,
  { useNewUrlParser: true, useCreateIndex: true },
  (err, db) => {
    if (!err) {
      console.log('Database connected');
    } else {
      console.log('Database ERR. Not connected');
    }
  }
);

mongoose.Promise = global.Promise;

module.exports = {
  User: require('../users/model'),
  Product: require('../products/model'),
  UserRating: require('../usersRating/model'),
};
