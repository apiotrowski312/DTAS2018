export default {
  env: process.env.NODE_ENV,
  connectionString:
    process.env.NODE_ENV !== 'test'
      ? 'mongodb://mongo:27017/alledrogo'
      : 'mongodb://localhost:27017/test',
  saltRounds: 10,
  secret: '2c3tf5fg3467tewcrfd124g43f67rwecx3d2f54',

  // ENUM
  categories: [
    'Home',
    'Work',
    'Freetime',
    'Outdoor',
    'Cooking',
    'Games',
    'Health',
    'Motorization',
  ],
  condition: ['New', 'Used'],
  postMethod: ['List', 'Package'],
  type: ['Buy now', 'Bid'],
};
