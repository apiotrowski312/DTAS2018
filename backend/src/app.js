import './products/commands';

import bodyParser from 'body-parser';
import cors from 'cors';
import errorHandler from './_helpers/error-handler';
import express from 'express';
import jwt from './_helpers/jwt';

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use(jwt());

app.use('/uploads', express.static('uploads'));

app.use('/users', require('./users/router'));
app.use('/products', require('./products/router'));
app.use('/rating', require('./usersRating/router'));

app.use(errorHandler);

module.exports = app;
