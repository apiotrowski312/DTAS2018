import db from '../_helpers/db.js';
import mongoose from 'mongoose';

const RatingModel = db.UserRating;

const Ratings = {};

Ratings.getAllAbout = async id => {
  return await RatingModel.find({ about: id });
};

Ratings.getAllPostedBy = async id => {
  return await RatingModel.find({ postedBy: id });
};

Ratings.create = async ({ body, user, files, params }) => {
  const rating = new RatingModel(body);

  if (user.sub === params.id) throw 'You cannot write your own rating';

  rating.postedBy = mongoose.Types.ObjectId(user.sub);
  rating.about = mongoose.Types.ObjectId(params.id);
  await rating.save();
};

Ratings._delete = async (userId, id) => {
  const rating = await RatingModel.findById(id);
  if (rating.postedBy !== userID) throw 'Rating not write by you';

  return await RatingModel.findByIdAndRemove(id);
};

module.exports = Ratings;
