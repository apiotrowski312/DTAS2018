import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const schema = new Schema({
  postedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  about: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  rating: {
    type: Number,
    required: true,
    min: [0, 'Too low rating'],
    max: [10, 'Too high rating'],
  },
  description: { type: String, required: false, maxlength: 500 },
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('UserRating', schema);
