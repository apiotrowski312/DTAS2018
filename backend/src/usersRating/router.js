import { _delete, addRating, getAllAbout, getAllPostedBy } from './handler';

import Ratings from './interface';
import express from 'express';

const router = express.Router();

router.get('/:id', getAllAbout(Ratings));
router.get('/by/:id', getAllPostedBy(Ratings));
router.post('/:id', addRating(Ratings));
router.delete('/rating/:id', _delete(Ratings));

module.exports = router;
