const getAllAbout = ProductsModel => (req, res, next) => {
  ProductsModel.getAllAbout(req.params.id)
    .then(rating => res.json(rating))
    .catch(err => res.sendStatus(404));
};

const getAllPostedBy = ProductsModel => (req, res, next) => {
  ProductsModel.getAllPostedBy(req.params.id)
    .then(rating => res.json(rating))
    .catch(err => res.sendStatus(404));
};

const addRating = ProductsModel => (req, res, next) => {
  ProductsModel.create(req)
    .then(() => res.json({}))
    .catch(err => next(err));
};

const _delete = ProductsModel => (req, res, next) => {
  ProductsModel._delete(req.user.sub, req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
};

module.exports = {
  getAllAbout,
  getAllPostedBy,
  addRating,
  _delete,
};
