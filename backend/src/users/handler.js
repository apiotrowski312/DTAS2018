const getAll = UsersModel => (req, res, next) => {
  UsersModel.getAll()
    .then(users => res.json(users))
    .catch(err => next(err));
};

const getById = UsersModel => (req, res, next) => {
  UsersModel.getById(req.params.id)
    .then(user => res.json(user))
    .catch(err => res.sendStatus(404));
};

const authenticate = UsersModel => (req, res, next) => {
  UsersModel.authenticate(req.body)
    .then(
      user =>
        user
          ? res.json(user)
          : res
              .status(400)
              .json({ message: 'Username or password is incorrect' })
    )
    .catch(err => next(err));
};

const register = UsersModel => (req, res, next) => {
  UsersModel.create(req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
};

const getCurrent = UsersModel => (req, res, next) => {
  UsersModel.getById(req.user.sub)
    .then(user => (user ? res.json(user) : res.sendStatus(404)))
    .catch(err => next(err));
};

const update = UsersModel => (req, res, next) => {
  UsersModel.update(req.params.id, req.body)
    .then(() => {
      return req.user.sub === req.params.id
        ? res.json({})
        : res.sendStatus(404);
    })
    .catch(err => next(err));
};

const _delete = UsersModel => (req, res, next) => {
  UsersModel._delete(req.params.id)
    .then(() => {
      return req.user.sub === req.params.id
        ? res.json({})
        : res.sendStatus(404);
    })
    .catch(err => next(err));
};

module.exports = {
  getAll,
  getById,
  authenticate,
  register,
  getCurrent,
  update,
  _delete,
};
