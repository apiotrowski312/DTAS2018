import argon2 from 'argon2';
import config from '../config.js';
import db from '../_helpers/db.js';
import jwt from 'jsonwebtoken';

const UserModel = db.User;

const Users = {};

Users.getAll = async () => {
  return await UserModel.find().select('firstName lastName id');
};

Users.getById = async id => {
  return await UserModel.findById(id).select(
    'id username firstName lastName email zipCode town street '
  );
};

Users.authenticate = async ({ username, password }) => {
  const user = await UserModel.findOne({ username });
  const isValid = await argon2.verify(user.hash, password);
  if (user && isValid) {
    const token = jwt.sign({ sub: user.id }, config.secret);
    return await {
      token,
      ...user,
    };
  }
};

Users.create = async userParam => {
  if (await UserModel.findOne({ username: userParam.username })) {
    throw 'Username "' + userParam.username + '" is already taken';
  }

  const user = new UserModel(userParam);

  if (userParam.password) {
    user.hash = await argon2.hash(userParam.password);
  } else {
    throw 'No password';
  }

  await user.save();
};

Users.update = async (id, userParam) => {
  const user = await UserModel.findById(id);

  if (!user) throw 'User not found';
  if (
    user.username !== userParam.username &&
    (await UserModel.findOne({ username: userParam.username }))
  ) {
    throw 'Username "' + userParam.username + '" is already taken';
  }

  if (userParam.password) {
    userParam.hash = await argon2.hash(userParam.password);
  }

  Object.assign(user, userParam);

  return await user.save();
};

Users._delete = async id => {
  return await UserModel.findByIdAndRemove(id);
};

module.exports = Users;
