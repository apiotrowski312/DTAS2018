import {
  _delete,
  authenticate,
  getAll,
  getById,
  getCurrent,
  register,
  update,
} from './handler';

import Users from './interface';
import express from 'express';

const router = express.Router();

router.get('/', getAll(Users));
router.get('/current', getCurrent(Users));
router.post('/authenticate', authenticate(Users));
router.post('/register', register(Users));

router.get('/:id', getById(Users));
router.put('/:id', update(Users));
router.delete('/:id', _delete(Users));

module.exports = router;
