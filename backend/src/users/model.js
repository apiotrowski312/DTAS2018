import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const validateEmail = email => {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const schema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    validate: [validateEmail, 'Please fill a valid email address'],
  },
  username: { type: String, unique: true, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  zipCode: { type: String, required: false },
  town: { type: String, required: false },
  street: { type: String, required: false },
  phone: { type: String, required: false },
  createdDate: { type: Date, default: Date.now },
  hash: { type: String, required: true },
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);
