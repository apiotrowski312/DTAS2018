import db from '../_helpers/db.js';

const ProductsModel = db.Product;

const checkIfExpired = async () => {
  const products = await ProductsModel.find({ active: true });
  products.forEach(cell => {
    if (cell.endDate < Date.now()) {
      cell.active = false;
      cell.save();
    }
  });
};

setInterval(() => {
  checkIfExpired();
}, 60 * 1000);
