import {
  _delete,
  bidUp,
  buyNow,
  createOffer,
  getAll,
  getAllPostedBy,
  getById,
  update,
} from './handler';

import Products from './interface';
import express from 'express';
import multer from 'multer';

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, 'uploads');
  },
  filename: (req, file, callback) => {
    callback(null, Date.now() + '-' + file.originalname);
  },
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 30,
  },
});

const router = express.Router();

router.get('/', getAll(Products));
router.post(
  '/createOffer',
  upload.array('productImages'),
  createOffer(Products)
);

router.get('/by/:id', getAllPostedBy(Products));

router.get('/:id', getById(Products));
router.put('/:id', update(Products));
router.delete('/:id', _delete(Products));
router.put('/buy/:id', buyNow(Products));
router.put('/bid/:id', bidUp(Products));
module.exports = router;
