const getAll = ProductsModel => (req, res, next) => {
  ProductsModel.getAll(req.query)
    .then(products => res.json(products))
    .catch(err => next(err));
};

const getById = ProductsModel => (req, res, next) => {
  ProductsModel.getById(req.params.id)
    .then(products => res.json(products))
    .catch(err => res.sendStatus(404));
};

const getAllPostedBy = ProductsModel => (req, res, next) => {
  ProductsModel.getAllPostedBy(req.params.id)
    .then(products => res.json(products))
    .catch(err => res.sendStatus(404));
};

const createOffer = ProductsModel => (req, res, next) => {
  console.log('OFERTA:', req);
  ProductsModel.create(req)
    .then(() => res.json({}))
    .catch(err => next(err));
};

const update = ProductsModel => (req, res, next) => {
  ProductsModel.update(req.user.sub, req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
};

const _delete = ProductsModel => (req, res, next) => {
  ProductsModel._delete(req.user.sub, req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
};

const buyNow = ProductsModel => (req, res, next) => {
  ProductsModel.buyNow(req.user.sub, req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
};

const bidUp = ProductsModel => (req, res, next) => {
  ProductsModel.bidUp(req.user.sub, req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
};

module.exports = {
  getAll,
  getById,
  getAllPostedBy,
  createOffer,
  update,
  _delete,
  buyNow,
  bidUp,
};
