import config from '../config.js';
import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const schema = new Schema({
  postedBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: { type: String, required: true, minlength: 10, maxlength: 50 },
  description: { type: String, required: true, minlength: 50 },
  category: {
    type: [String],
    required: true,
    enum: config.categories,
  },
  condition: { type: String, required: true, enum: config.condition },
  quantity: { type: Number, required: true },
  price: { type: Number, required: true },
  publicationDate: { type: Date, default: Date.now },
  postMethod: {
    type: [[String, Number]],
    required: true,
    validate: {
      validator: data =>
        data.every(cell => config.postMethod.includes(cell[0])),
      message: prop => prop.value + ' is not valid enum',
    },
  },
  productImages: { type: [String], required: false },
  endDate: { type: Date, required: true },
  active: { type: Boolean, default: true },
  boughtBy: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'User',
    required: true,
  },
  type: { type: String, required: true, enum: config.type },
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Product', schema);
