import db from '../_helpers/db.js';
import mongoose from 'mongoose';
import SETTINGS from '../config';
const ProductsModel = db.Product;

const Products = {};

Products.getAll = async query => {
  if (Object.keys(query).length === 0) {
    return await ProductsModel.find();
  }

  const queryForFinding = {};
  Object.entries(query).map(([id, value]) => {
    switch (id) {
      case 'price':
        if (value.length === 1) {
          queryForFinding.price = {
            $gte: value[0],
          };
        } else {
          queryForFinding.price = {
            $gte: value[0],
            $lte: value[1],
          };
        }
        break;
      case 'category':
        queryForFinding.category = {
          $in: value,
        };
        break;
      case 'condition':
        queryForFinding.condition = {
          $in: value,
        };
        break;
      case 'title':
        queryForFinding.title = `/${value}/`;
        break;
    }
  });

  return await ProductsModel.find(queryForFinding);
};

Products.getById = async id => {
  return await ProductsModel.findById(id);
};

Products.getAllPostedBy = async id => {
  return await ProductsModel.find({ postedBy: id });
};

Products.create = async ({ body, user, files }) => {
  const product = new ProductsModel(body);
  console.log(body);
  console.log(files);
  if (files) {
    const filePaths = files.map(file => file.path);
    product.productImages = filePaths;
  } else {
    product.productImages = null;
  }

  product.postedBy = mongoose.Types.ObjectId(user.sub);
  await product.save();
};

Products.update = async (userId, id, params) => {
  const product = await ProductsModel.findById(id);

  if (!product) throw 'Product not found';
  if (product.postedBy != userId) throw 'Product not owned by you';

  Object.assign(product, params);

  return await product.save();
};

Products._delete = async (userId, id) => {
  const product = await ProductsModel.findById(id);
  if (product.postedBy !== userID) throw 'Product not owned by you';

  return await ProductsModel.findByIdAndRemove(id);
};

Products.buyNow = async (userId, id, params) => {
  const product = await ProductsModel.findById(id);

  if (!product) throw 'Product not found';
  if (product.postedBy == userId) throw 'You cannot buy your own product';
  if (product.type != SETTINGS.type[0]) throw 'You cannot buy bid product';
  if (product.active == false) throw "Time's up, it is no more on sale";
  if (product.quantity < params.quantity)
    throw 'There are too less products to buy by you';

  let quantity = params.quantity;
  while (quantity) {
    product.quantity -= 1;
    product.boughtBy.push(mongoose.Types.ObjectId(userId));
    quantity -= 1;
  }

  if (product.quantity == 0) product.active = false;

  return await product.save();
};

Products.bidUp = async (userId, id, params) => {
  const product = await ProductsModel.findById(id);

  if (!product) throw 'Product not found';
  if (product.postedBy == userId) throw 'You cannot bid on your own product';
  if (product.type != SETTINGS.type[1]) throw 'You cannot bid buy now producy';
  if (product.active == false) throw "Time's up, it is no more on sale";
  if (product.price >= params.price) throw 'Your bid is to small to accept';

  product.price = params.price;
  product.boughtBy = [mongoose.Types.ObjectId(userId)];

  return await product.save();
};

module.exports = Products;
