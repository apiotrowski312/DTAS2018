# Backend for Allegro like app

## Technology stack:

- NodeJS
- MongoDB
- Docker

###

- docker-compose up app

### Rest api for:

- Sign in
- Sign up
- change password
- change user data
- add/edit/remove auction
- buy now (item on auction)
- bid up (item on auction)
- filter offers

Header:
Authorization: Bearer TOKEN

### Endpoints

Method | path | description
--- | --- | ---
GET | `users/` | all users
GET | `users/current` | current log user
POST | `users/authenticate` | login user
POST | `users/register` | register user
GET | `users/:id` | user with ID
PUT | `users/:id` | update user
DELETE | `users/:id` | delete user
GET | `products` | all product
POST | `**product/createOffer**` | create new product
GET | `products/:id` | get product
PUT | `products/:id` | update product
DELETE | `products/:id` | delete product

### Filters for products

You can use this query strings on GET `products`

Query | Find:
--- | ---
`title=XXX`  | products with XXX title
`price[0]=XXX`  | products with higher price than XXX
`price[1]=XXX`  | products with lower price than XXX
`category[]=X` | product with category X
`condition=X` | product with condition X


#### User

 - `users/register` `users/:id`
  ```json
  {
	  "username": string,
    "password": string,
    "firstName": string,
    "lastName": string,
    "email": string,
    "zipCode": string,
    "town": string,
    "street": string,
    "phone": string,
  }
  ```

 - `users/authenticate`
  ```json
  {
    "username": string,
    "password": string
  }
  ```

  #### Product

  You can find enums inside `./src/config.js`

   - `product/createOffer` `products/:id`
  ```json
{
	"title": string(10-50 chars),
	"description": string(50+ chars),
	"category": [enum],
	"condition": enum,
	"quantity": number,
  "postMethod": [[enum, number]],
  "price": number,
  "type": enum,
}
  ```

  - `product/bid/:id`
  ```json
{
	"quantity": number,
}
  ```

  - `product/bid/:id`
  ```json
{
	"price": number,
}
  ```