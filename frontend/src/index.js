import * as serviceWorker from './serviceWorker';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Redirect, Route, BrowserRouter as Router } from 'react-router-dom';

import AddProductForm from './screen/AddProductForm';
import AuthScreen from './screen/AuthScreen';
import CssBaseline from '@material-ui/core/CssBaseline';
import { I18nextProvider } from 'react-i18next';
import { LandingPage } from './screen';
import { Navbar } from './common';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import SETTINGS from './config';
import configureStore from './store/store';
import i18n from './i18n';
import ProfileScreen from './screen/ProfileScreen';
import ProductOverviewScreen from './screen/ProductOverviewScreen';

const theme = createMuiTheme({
  breakpoints: {
    keys: ['xxs', 'xs', 'sm', 'md', 'lg', 'xl'],
    values: {
      xxs: 0,
      xs: 414,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
});

ReactDOM.render(
  <React.Fragment>
    <CssBaseline />
    <Provider store={configureStore()}>
      <MuiThemeProvider theme={theme}>
        <I18nextProvider i18n={i18n}>
          <Router>
            <div>
              <Navbar />
              <div>
                <Route exact path="/" render={() => <Redirect to={SETTINGS.path.landingPage} />} />
                <Route exact path={SETTINGS.path.auth} component={AuthScreen} />
                <Route exact path={SETTINGS.path.landingPage} component={LandingPage} />
                <Route exact path={SETTINGS.path.user} component={ProfileScreen} />
                <Route exact path={SETTINGS.path.getProduct} component={ProductOverviewScreen} />
                <Route exact path={SETTINGS.path.addProd} component={AddProductForm} />
              </div>
            </div>
          </Router>
        </I18nextProvider>
      </MuiThemeProvider>
    </Provider>
  </React.Fragment>,
  document.getElementById('root'),
);
serviceWorker.unregister();
