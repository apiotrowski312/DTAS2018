import * as actions from '../store/prod/actions';

import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControlLabel,
  FormGroup,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Typography,
} from '@material-ui/core';
import React, { PureComponent } from 'react';
import { isEmpty, isNumeric, matches } from 'validator';
import { withStyles, withTheme } from '@material-ui/core/styles';

import { DateFormatInput } from 'material-ui-next-pickers';
import { Input } from '../common';
import { Line } from 'rc-progress';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import request from 'superagent';
import { withNamespaces } from 'react-i18next';

const styles = theme => ({
  registerWrapper: {
    minHeight: '100%',
    textAlign: 'center',
    margin: '0.5rem',
    '& h1': {
      paddingTop: '1rem',
    },
  },
  registerForm: {
    display: 'flex',
    flexDirection: 'column',
    '& > div': {
      margin: '0.32rem 1rem',
    },
  },
  placeForm: {
    display: 'flex',
    flexDirection: 'row',
  },
  registerBtn: {
    backgroundColor: theme.palette.primary.main,
    margin: '1rem 1rem 2rem 1rem',
  },
  otherBtn: {
    margin: '0% 45% 0 45%',
  },
  errorText: {
    color: theme.palette.error.main,
    marginTop: '-1.25rem',
  },
  dialog: {
    backgroundColor: theme.palette.primary.contrastText,
  },
  formHelper: {
    marginTop: 0,
    marginLeft: theme.spacing.unit * 2,
  },
  inputLabelForm: {
    textAlign: 'left',
    marginLeft: theme.spacing.unit * 2,
  },
  datePicker: {
    width: '98%',
  },
  input: {
    display: 'none',
  },
});

const defaultState = {
  title: '',
  description: '',
  category: [],
  condition: '',
  quantity: '',
  postMethod: [],
  price: '',
  type: '',
  endDate: new Date(),
  isDialogOpen: false,
  isDialogEmptyOpen: false,
  isDialogTitleShort: false,
  isDialogDescriptionShort: false,
  isDialogTitleLong: false,
  isQuantityZero: false,
  isPriceBelow: false,
  isQuantityInt: false,
  productImages: [],
  checkedList: false,
  checkedPackage: false,
  pML0: 'List',
  pML1: '',
  pMP0: 'Package',
  pMP1: '',
};

class AddProductForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = defaultState;
  }

  fileChangedHandler = event => {
    this.setState({ productImages: event.target.files[0] });
  };

  onChangeDate = date => {
    this.setState({ endDate: date });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleAdd = event => {
    event.preventDefault();
    //make sth about addProduct in actions
    const { addProduct } = this.props;

    let isValid = this.title.isValid();
    isValid = this.description.isValid();
    if (this.title.props.value.trim() === '' || this.description.props.value.trim() === '') {
      this.setState({ isDialogEmptyOpen: true });
    } else if (this.title.props.value.length < 10) {
      this.setState({ isDialogTitleShort: true });
    } else if (this.title.props.value.length > 50) {
      this.setState({ isDialogTitleLong: true });
    } else if (this.description.props.value.length < 50) {
      this.setState({ isDialogDescriptionShort: true });
    } else if (this.quantity.props.value <= 0) {
      this.setState({ isQuantityZero: true });
    } else if (this.price.props.value < 0) {
      this.setState({ isPriceBelow: true });
    } else if (!Number.isInteger(Number(this.quantity.props.value))) {
      this.setState({ isQuantityInt: true });
    } else if (isValid) {
      const { isDialogOpen, productImages, category, postMethod, ...registerForm } = this.state;
      const postMethodArray = [];
      if (this.state.checkedList) {
        postMethodArray.push(['List', this.state.pML1]);
      }
      if (this.state.checkedPackage) {
        postMethodArray.push(['Package', this.state.pMP1]);
      }

      addProduct({ productImages, category, postMethod: postMethodArray, ...registerForm }).then(
        added => {
          if (added) {
            this.setState({ ...defaultState, isDialogOpen: true }, () => {});
          }
        },
      );
    }
  };

  handleClose = () =>
    this.setState({
      isDialogOpen: false,
      isDialogEmptyOpen: false,
      isDialogDescriptionShort: false,
      isDialogTitleShort: false,
      isDialogTitleLong: false,
      isQuantityZero: false,
      isPriceBelow: false,
      isQuantityInt: false,
      checkedList: false,
      checkedPackage: false,
    });

  render() {
    const { t, classes, errorText } = this.props;
    const {
      title,
      description,
      category,
      condition,
      quantity,
      postMethod,
      price,
      type,
      endDate,
      productImages,
      isDialogOpen,
      isDialogEmptyOpen,
      isDialogDescriptionShort,
      isDialogTitleShort,
      isDialogTitleLong,
      isQuantityZero,
      isPriceBelow,
      isQuantityInt,
      checkedList,
      checkedPackage,
      pML0,
      pML1,
      pMP0,
      pMP1,
    } = this.state;
    return (
      <React.Fragment>
        <Paper className={classes.registerWrapper}>
          <Typography variant={'headline'}>{t('addHeader')}</Typography>
          <form
            className={classes.registerForm}
            encType="multipart/form-data"
            onSubmit={this.handleAdd}>
            <InputLabel shrink htmlFor="title" className={classes.inputLabelForm}>
              {t('titleDes')}
            </InputLabel>
            <Input
              id="title"
              onRef={input => (this.title = input)}
              value={title}
              validators={{
                emptyError: value => !isEmpty(value),
              }}
              label={t('title')}
              onChange={title => this.setState({ title })}
            />
            <InputLabel shrink htmlFor="description" className={classes.inputLabelForm}>
              {t('descriptionDes')}
            </InputLabel>
            <Input
              id="description"
              onRef={input => (this.description = input)}
              value={description}
              type="description"
              validators={{
                emptyError: value => !isEmpty(value),
              }}
              label={t('description')}
              onChange={description => this.setState({ description })}
            />
            <InputLabel shrink htmlFor="category-select" className={classes.inputLabelForm}>
              {t('category')}
            </InputLabel>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    value="Home"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('home')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value="Work"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('work')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value="Freetime"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('freetime')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value="Outdoor"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('outdoor')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value="Cooking"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('cooking')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value="Games"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('games')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value="Health"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('health')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value="Motorization"
                    color="primary"
                    onChange={e => {
                      const options = this.state.category;
                      let index;
                      if (e.target.checked) {
                        options.push(e.target.value);
                      } else {
                        index = options.indexOf(+e.target.value);
                        options.splice(index, 1);
                      }
                      this.setState({ category: options });
                    }}
                  />
                }
                label={t('motorization')}
              />
            </FormGroup>
            <InputLabel shrink htmlFor="condition-select" className={classes.inputLabelForm}>
              {t('condition')}
            </InputLabel>
            <Select
              value={this.state.condition}
              onChange={this.handleChange}
              displayEmpty
              inputProps={{
                name: 'condition',
                id: 'condition-select',
              }}>
              <MenuItem selected disabled value="">
                {t('none')}
              </MenuItem>
              <MenuItem value="New">{t('newItem')}</MenuItem>
              <MenuItem value="Used">{t('used')}</MenuItem>
            </Select>
            <InputLabel shrink htmlFor="quantity" className={classes.inputLabelForm}>
              {t('quantity')}
            </InputLabel>
            <Input
              id="quantity"
              type="number"
              onRef={input => (this.quantity = input)}
              value={quantity}
              validators={{
                emptyError: value => !isEmpty(quantity),
              }}
              label={t('quantity')}
              onChange={quantity => this.setState({ quantity })}
            />
            <InputLabel shrink htmlFor="postMethod-select" className={classes.inputLabelForm}>
              {t('postMethod')}
            </InputLabel>
            <FormGroup id="postMethod-select">
              <FormControlLabel
                control={
                  <Checkbox
                    value={'List'}
                    color="primary"
                    onChange={e => {
                      this.setState({ checkedList: e.target.value });
                    }}
                  />
                }
                label={t('list')}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    value={'Package'}
                    color="primary"
                    onChange={e => {
                      this.setState({ checkedPackage: e.target.value });
                    }}
                  />
                }
                label={t('package')}
              />
            </FormGroup>
            <Input
              id="priceList"
              type="number"
              ref={input => (this.pML1 = input)}
              value={pML1}
              validators={{
                emptyError: value => !isEmpty(price),
              }}
              label={t('priceList')}
              onChange={pML1 => this.setState({ pML1 })}
              disabled={!this.state.checkedList}
            />
            <Input
              id="pricePackage"
              type="number"
              ref={input => (this.pMP1 = input)}
              value={pMP1}
              validators={{
                emptyError: value => !isEmpty(price),
              }}
              label={t('pricePackage')}
              onChange={pMP1 => this.setState({ pMP1 })}
              disabled={!this.state.checkedPackage}
            />
            <InputLabel shrink htmlFor="price" className={classes.inputLabelForm}>
              {t('price')}
            </InputLabel>
            <Input
              id="price"
              type="number"
              onRef={input => (this.price = input)}
              value={price}
              validators={{
                emptyError: value => !isEmpty(price),
                //emailError: value => isEmail(value),
              }}
              label={t('price')}
              onChange={price => this.setState({ price })}
            />
            <InputLabel shrink htmlFor="type-select" className={classes.inputLabelForm}>
              {t('type')}
            </InputLabel>
            <Select
              value={this.state.type}
              onChange={this.handleChange}
              displayEmpty
              inputProps={{
                name: 'type',
                id: 'type-select',
              }}>
              <MenuItem selected disabled value="">
                {t('none')}
              </MenuItem>
              <MenuItem value={'Buy now'}>{t('buyNow')}</MenuItem>
              <MenuItem value={'Bid'}>{t('bid')}</MenuItem>
            </Select>
            <DateFormatInput
              name="date-input"
              label={t('endDate')}
              value={endDate}
              onChange={this.onChangeDate}
              min={endDate}
              className={classes.datePicker}
              fullWidth={true}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <input
              accept="image/*"
              className={classes.input}
              id="contained-button-file"
              multiple
              type="file"
              onChange={e => {
                let prod = this.state.productImages;
                prod.push(e.target.files[0]);
                this.setState({ productImages: prod });
              }}
            />
            <label htmlFor="contained-button-file">
              <Button
                variant="contained"
                component="span"
                className={classes.button}
                onClick={this.uploadHandler}>
                {t('upload')}
              </Button>
              <Line
                percent={this.state.completed}
                strokeWidth="0.5"
                strokeColor="#2db7f5"
                strokeLinecap="square"
              />
            </label>
            <Button size="large" className={classes.registerBtn} type="submit">
              {t('add')}
            </Button>
          </form>
          <Typography className={classes.errorText}>{t(errorText)}</Typography>
        </Paper>
        <Dialog open={isDialogOpen} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('addProductDialogTitle')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('addProductDialogBody')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
        <Dialog open={isDialogEmptyOpen} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('errorEmpty')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
        <Dialog open={isDialogTitleShort} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('errorTitleShort')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
        <Dialog open={isDialogDescriptionShort} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('errorDescriptionShort')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
        <Dialog open={isDialogTitleLong} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('errorTitleLong')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
        <Dialog open={isQuantityZero} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('errorQuantityBelow')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
        <Dialog open={isPriceBelow} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('errorPriceBelow')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
        <Dialog open={isQuantityInt} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('errorQuantityInt')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('addProductDialogOK')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  errorText: state.auth.errorText,
});

AddProductForm.propTypes = {
  errorText: PropTypes.string.isRequired,
};

export default withNamespaces('AuthScreen')(
  withStyles(styles)(
    connect(
      mapStateToProps,
      { addProduct: actions.addProduct },
    )(AddProductForm),
  ),
);
