import React, { PureComponent } from 'react';

import { Button } from '@material-ui/core';
import { ProductPromoSmall } from '../common';
import { withNamespaces } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import AuthScreen from './AuthScreen';
import * as actions from '../store/product/actions';
import Product from '../common/Product';
import { connect } from 'react-redux';
import * as actionsUser from '../store/user/actions';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import { Link } from 'react-router-dom';
import config from '../config';

const styles = theme => ({
  exampleStyle: {
    width: '100vw',
    height: '15rem',
    backgroundColor: theme.palette.primary.secondary,
  },
  products: {
    '& div': {
      margin: '1rem',
    },
  },
  card: {
    maxWidth: 400,
    margin: '1%',
  },
  media: {
    height: 250,
  },
  root: {
    flexGrow: 1,
  },
  productList: {
    marginTop: '5%',
  },
  paper: {
    padding: theme.spacing.unit * 3,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  crop: {
    lineHeight: 1,
    overflow: 'hidden',
  },
  badge: {
    top: '0%',
    right: 60,
    width: 55,
    height: 35,
    // The border color match the background color.
    border: `0px solid ${
      theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900]
    }`,
  },
});

class LandingPage extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
    };
  }

  componentWillMount() {
    this.getData();
  }

  getData() {
    this.props.getProducts(this.props.match.params.id).then(productData => {
      this.setState({
        products: productData ? productData : [],
      });
    });
  }

  render() {
    const { products } = this.state;
    const { t, classes } = this.props;
    return (
      <div className={classes.productList}>
        <Grid container spacing={40} justify="center">
          {products.map(product => (
            <div>
              <Badge
                badgeContent={t(product.condition)}
                color="primary"
                classes={{ badge: classes.badge }}>
                <Grid item xs={10}>
                  <Card className={classes.card}>
                    <CardActionArea>
                      <CardMedia
                        className={classes.media}
                        image={`${config.apiUrl}/${
                          product.productImages ? product.productImages[0] : 'null'
                        }`}
                        title={product.title}
                      />
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="h5"
                          component="p"
                          className={classes.crop}>
                          {product.title}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2">
                          {t('price')}: {product.price}
                        </Typography>
                        <Typography component="p" className={classes.crop}>
                          {product.description.split(' ', 15).join(' ')} ...
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                    <CardActions>
                      <Link to={'/product/' + product.id}>
                        <Button size="small" color="primary">
                          {t('seeMore')}
                        </Button>
                      </Link>
                    </CardActions>
                  </Card>
                </Grid>
              </Badge>
            </div>
          ))}
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.product.error,
  errorTextUser: state.user.errorText,
});

export default withStyles(styles)(
  withNamespaces('LandingPage')(
    connect(
      mapStateToProps,
      { getProducts: actions.getProducts, getUserData: actionsUser.getUserData },
    )(LandingPage),
  ),
);
