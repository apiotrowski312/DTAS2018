import * as actions from '../../store/user/actions';

import {
  Button,
  Paper,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Card,
  withStyles,
  IconButton,
  CardHeader,
  CardContent,
  CardActions,
  Collapse,
  Avatar,
} from '@material-ui/core';
import React, { PureComponent } from 'react';
import { Input } from '../../common';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { isEmpty, isEmail, isNumeric, matches } from 'validator';
import classnames from 'classnames';
import purple from '@material-ui/core/colors/purple';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Assignment from '@material-ui/icons/Assignment';
import Save from '@material-ui/icons/Save';

const styles = theme => ({ 
  registerWrapper: {
    minHeight: '95%',
    textAlign: 'center',
    margin: '0.5rem',
    '& h1': {
      paddingTop: '1rem',
    },
  },
  updateForm: {
    display: 'flex',
    flexDirection: 'column',
    '& > div': {
      margin: '0.32rem 1rem',
    },
  },
  placeForm: {
    display: 'flex',
    flexDirection: 'row',
  },
  registerBtn: {
    backgroundColor: theme.palette.primary.main,
    margin: '1rem 1rem 2rem 1rem',
    color: '#fcfcfc'
  },
  errorText: {
    color: theme.palette.error.main,
    marginTop: '-1.25rem',
  },
  dialog: {
    backgroundColor: theme.palette.primary.contrastText,
  },
  card: {
    maxWidth: '100%',
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: purple[500],
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class ProfileForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      firstName: '',
      lastName: '',
      email: '',
      zipCode: '',
      town: '',
      street: '',
      isDialogOpen: false,
    };
  }

  componentWillMount() {
    this.props.getUserData().then(userData => {
      this.setState({
        username: userData.username,
        firstName: userData.firstName,
        lastName: userData.lastName,
        email: userData.email,
        zipCode: userData.zipCode,
        town: userData.town,
        street: userData.street,
        isDialogOpen: false,
      });
    });
  }

  handleUpdate = event => {
    event.preventDefault();
    const { updateUser } = this.props;

    let isValid = this.username.isValid();
    isValid = this.password.isValid();
    isValid = this.firstName.isValid();
    isValid = this.lastName.isValid();
    isValid = this.email.isValid();
    isValid = this.zipCode.isValid();
    isValid = this.town.isValid();
    isValid = this.street.isValid();

    if (isValid) {
      const { isDialogOpen, ...updateForm } = this.state;
      updateUser(this.props.userData.id, updateForm).then(registed => {
        if (registed) {
          this.setState({ isDialogOpen: true }, () => {
            this.username.clear();
            this.password.clear();
            this.firstName.clear();
            this.lastName.clear();
            this.email.clear();
            this.zipCode.clear();
            this.town.clear();
            this.street.clear();
          });
        } else {
          this.setState({ password: '' }, () => {
            this.password.clear();
          });
        }
      });
    }
  };

  handleClose = () => this.setState({ isDialogOpen: false });

  state = { expanded: false };
  
    handleExpandClick = () => {
      this.setState(state => ({ expanded: !state.expanded }));
    };

  render() {
    const { t, classes, errorText, pending } = this.props;
    const {
      username,
      password,
      firstName,
      lastName,
      email,
      zipCode,
      town,
      street,
      isDialogOpen,
    } = this.state;

    return (
      <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="Edit" className={classes.avatar}>
            <Assignment />
          </Avatar>
        }
        title={t('mainTitle')}
      />
      <CardContent>
        <Typography component="p">
          {t('shortDesc')}
        </Typography>
      </CardContent>
      <CardActions className={classes.actions} disableActionSpacing>
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: this.state.expanded,
          })}
          onClick={this.handleExpandClick}
          aria-expanded={this.state.expanded}
          aria-label="Show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <React.Fragment>
            <Paper className={classes.registerWrapper}>
              <Typography variant={'headline'}>{t('registerHeader')}</Typography>
              <form className={classes.updateForm} onSubmit={this.handleUpdate}>
                <Input
                  onRef={input => (this.username = input)}
                  value={username}
                  validators={{
                    emptyError: value => !isEmpty(value),
                  }}
                  label={t('username')}
                  onChange={username => this.setState({ username })}
                />
                <Input
                  onRef={input => (this.password = input)}
                  value={password}
                  type="password"
                  validators={{
                    emptyError: value => !isEmpty(value),
                    toShortPassword: value => matches(value, /[a-zA-Z0-9]{8,}/i),
                  }}
                  label={t('password')}
                  onChange={password => this.setState({ password })}
                />
                <Input
                  onRef={input => (this.firstName = input)}
                  value={firstName}
                  validators={{
                    emptyError: value => !isEmpty(value),
                  }}
                  label={t('firstName')}
                  onChange={firstName => this.setState({ firstName })}
                />
                <Input
                  onRef={input => (this.lastName = input)}
                  value={lastName}
                  validators={{
                    emptyError: value => !isEmpty(value),
                  }}
                  label={t('lastName')}
                  onChange={lastName => this.setState({ lastName })}
                />
                <Input
                  onRef={input => (this.email = input)}
                  value={email}
                  validators={{
                    emptyError: value => !isEmpty(value),
                    emailError: value => isEmail(value),
                  }}
                  label={t('email')}
                  onChange={email => this.setState({ email })}
                />
                <div className={classes.placeForm}>
                  <Input
                    onRef={input => (this.zipCode = input)}
                    value={zipCode}
                    validators={{
                      emptyError: value => !isEmpty(value),
                      notZipCode: value => matches(value, /\d\d-\d\d\d/i),
                    }}
                    label={t('zipCode')}
                    onChange={zipCode => this.setState({ zipCode })}
                  />
                  <Input
                    onRef={input => (this.town = input)}
                    value={town}
                    validators={{
                      emptyError: value => !isEmpty(value),
                    }}
                    label={t('town')}
                    onChange={town => this.setState({ town })}
                  />
                  <Input
                    onRef={input => (this.street = input)}
                    value={street}
                    validators={{
                      emptyError: value => !isEmpty(value),
                    }}
                    label={t('street')}
                    onChange={street => this.setState({ street })}
                  />
                </div>
                <Button size="large" className={classes.registerBtn} type="submit" variant="extendedFab">
                  {t('register')}<Save/>
                </Button>
              </form>
              <Typography className={classes.errorText}>{t(errorText)}</Typography>
            </Paper>
            <Dialog open={isDialogOpen} onClose={this.handleCloseDialog}>
              <div className={classes.dialog}>
                <DialogTitle>{t('userRegisterDialogTitle')}</DialogTitle>
                <DialogContent>
                  <DialogContentText>{t('userRegisterDialogBody')}</DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleClose} variant="extendedFab" aria-label="Ok" className={classes.button}>
                    {t('userRegisterDialogOk')}
                  </Button>
                </DialogActions>
              </div>
            </Dialog>
          </React.Fragment>
      </CardContent>
        </Collapse>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  errorText: state.user.errorText,
  userData: state.user.userData,
  pending: state.user.pending,
});

ProfileForm.propTypes = {
  errorText: PropTypes.string.isRequired,
  userData: PropTypes.object,
};

export default withNamespaces('ProfileScreen')(
  withStyles(styles)(
    connect(
      mapStateToProps,
      { getUserData: actions.getUserData, updateUser: actions.updateUser },
    )(ProfileForm),
  ),
);
