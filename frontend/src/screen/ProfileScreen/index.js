import React from 'react';

import UserForm from './ProfileForm';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  profileScreen: {
    display: 'flex',
    flexDirection: 'row',
    '& > *': {
      marginTop: '1rem',
      flex: '1',
    },
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  card: {
    [theme.breakpoints.up('lg')]: {
      paddingLeft: '15%',
      paddingRight: '15%',
    },
    [theme.breakpoints.up('md')]: {
      paddingLeft: '7.5%',
      paddingRight: '7.5%',
    },
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '0%',
      paddingRight: '0%',
    },
  },
});

const ProfileScreen = ({ text, classes }) => (
  <div className={classes.profileScreen}>
      <div className={classes.card}>
        <UserForm />
      </div>
  </div>
);

ProfileScreen.propTypes = {
  text: PropTypes.string,
};

ProfileScreen.defaultProps = {
  text: 'Nie ustawili mi defaultPropsa',
};

export default withStyles(styles)(ProfileScreen);
