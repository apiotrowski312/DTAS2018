import * as actions from '../../store/auth/actions';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  Typography,
} from '@material-ui/core';
import React, { PureComponent } from 'react';
import { isEmail, isEmpty, isNumeric, matches } from 'validator';

import { Input } from '../../common';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  registerWrapper: {
    minHeight: '95%',
    textAlign: 'center',
    margin: '0.5rem',
    '& h1': {
      paddingTop: '1rem',
    },
  },
  registerForm: {
    display: 'flex',
    flexDirection: 'column',
    '& > div': {
      margin: '0.32rem 1rem',
    },
  },
  placeForm: {
    display: 'flex',
    flexDirection: 'row',
  },
  registerBtn: {
    backgroundColor: theme.palette.primary.main,
    margin: '1rem 1rem 2rem 1rem',
  },
  errorText: {
    color: theme.palette.error.main,
    marginTop: '-1.25rem',
  },
  dialog: {
    backgroundColor: theme.palette.primary.contrastText,
  },
});

const defaultState = {
  username: '',
  password: '',
  firstName: '',
  lastName: '',
  email: '',
  zipCode: '',
  town: '',
  street: '',
  isDialogOpen: false,
};

class RegForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = defaultState;
  }

  handleRegister = event => {
    event.preventDefault();
    const { registerUser } = this.props;

    let isValid = this.username.isValid();
    isValid = this.password.isValid();
    isValid = this.firstName.isValid();
    isValid = this.lastName.isValid();
    isValid = this.email.isValid();
    isValid = this.zipCode.isValid();
    isValid = this.town.isValid();
    isValid = this.street.isValid();

    if (isValid) {
      const { isDialogOpen, ...registerForm } = this.state;
      registerUser(registerForm).then(registed => {
        if (registed) {
          this.setState({ ...defaultState, isDialogOpen: true }, () => {
            this.username.clear();
            this.password.clear();
            this.firstName.clear();
            this.lastName.clear();
            this.email.clear();
            this.zipCode.clear();
            this.town.clear();
            this.street.clear();
          });
        } else {
          this.setState({ password: '' }, () => {
            this.password.clear();
          });
        }
      });
    }
  };

  handleClose = () => this.setState({ isDialogOpen: false });

  render() {
    const { t, classes, errorText } = this.props;
    const {
      username,
      password,
      firstName,
      lastName,
      email,
      zipCode,
      town,
      street,
      isDialogOpen,
    } = this.state;

    return (
      <React.Fragment>
        <Paper className={classes.registerWrapper}>
          <Typography variant={'headline'}>{t('registerHeader')}</Typography>
          <form className={classes.registerForm} onSubmit={this.handleRegister}>
            <Input
              onRef={input => (this.username = input)}
              value={username}
              validators={{
                emptyError: value => !isEmpty(value),
              }}
              label={t('username')}
              onChange={username => this.setState({ username })}
            />
            <Input
              onRef={input => (this.password = input)}
              value={password}
              type="password"
              validators={{
                emptyError: value => !isEmpty(value),
              }}
              label={t('password')}
              onChange={password => this.setState({ password })}
            />
            <Input
              onRef={input => (this.firstName = input)}
              value={firstName}
              validators={{
                emptyError: value => !isEmpty(value),
              }}
              label={t('firstName')}
              onChange={firstName => this.setState({ firstName })}
            />
            <Input
              onRef={input => (this.lastName = input)}
              value={lastName}
              validators={{
                emptyError: value => !isEmpty(value),
              }}
              label={t('lastName')}
              onChange={lastName => this.setState({ lastName })}
            />
            <Input
              onRef={input => (this.email = input)}
              value={email}
              validators={{
                emptyError: value => !isEmpty(value),
                emailError: value => isEmail(value),
              }}
              label={t('email')}
              onChange={email => this.setState({ email })}
            />
            <div className={classes.placeForm}>
              <Input
                onRef={input => (this.zipCode = input)}
                value={zipCode}
                validators={{
                  emptyError: value => !isEmpty(value),
                  notZipCode: value => matches(value, /\d\d-\d\d\d/i),
                }}
                label={t('zipCode')}
                onChange={zipCode => this.setState({ zipCode })}
              />
              <Input
                onRef={input => (this.town = input)}
                value={town}
                validators={{
                  emptyError: value => !isEmpty(value),
                }}
                label={t('town')}
                onChange={town => this.setState({ town })}
              />
              <Input
                onRef={input => (this.street = input)}
                value={street}
                validators={{
                  emptyError: value => !isEmpty(value),
                }}
                label={t('street')}
                onChange={street => this.setState({ street })}
              />
            </div>
            <Button size="large" className={classes.registerBtn} type="submit">
              {t('register')}
            </Button>
          </form>
          <Typography className={classes.errorText}>{t(errorText)}</Typography>
        </Paper>
        <Dialog open={isDialogOpen} onClose={this.handleCloseDialog}>
          <div className={classes.dialog}>
            <DialogTitle>{t('userRegisterDialogTitle')}</DialogTitle>
            <DialogContent>
              <DialogContentText>{t('userRegisterDialogBody')}</DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary" autoFocus>
                {t('userRegisterDialogOk')}
              </Button>
            </DialogActions>
          </div>
        </Dialog>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  errorText: state.auth.errorText,
});

RegForm.propTypes = {
  errorText: PropTypes.string.isRequired,
};

export default withNamespaces('AuthScreen')(
  withStyles(styles)(
    connect(
      mapStateToProps,
      { registerUser: actions.registerUser },
    )(RegForm),
  ),
);
