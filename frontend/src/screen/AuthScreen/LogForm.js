import * as actions from '../../store/auth/actions';

import { Button, Paper, Typography } from '@material-ui/core';
import React, { PureComponent } from 'react';

import { Input } from '../../common';
import PropTypes from 'prop-types';
import SETTINGS from '../../config';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { isEmpty } from 'validator';

const styles = theme => ({
  loginWrapper: {
    textAlign: 'center',
    margin: '0.5rem',
    '& h1': {
      paddingTop: '1rem',
    },
  },
  loginForm: {
    display: 'flex',
    flexDirection: 'column',
    '& > div': {
      margin: '0.32rem 1rem',
    },
  },
  loginBtn: {
    background: theme.palette.primary.main,
    margin: '1rem 1rem 2rem 1rem',
  },
  errorText: {
    color: theme.palette.error.main,
    marginTop: '-1.25rem',
  },
});

class LogForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }

  handleLogin = event => {
    event.preventDefault();
    const { username, password } = this.state;
    const { loginUser, history } = this.props;

    let isValid = this.username.isValid();
    isValid = this.password.isValid();

    if (isValid) {
      loginUser({ username, password }).then(token => {
        if (token) {
          history.push(SETTINGS.path.landingPage);
        } else {
          this.setState({ password: '' }, () => {
            this.password.clear();
          });
        }
      });
    }
  };

  render() {
    const { t, classes, errorText } = this.props;
    const { username, password } = this.state;
    return (
      <Paper className={classes.loginWrapper}>
        <Typography variant={'headline'}>{t('loginHeader')}</Typography>
        <form className={classes.loginForm} onSubmit={this.handleLogin}>
          <Input
            onRef={input => (this.username = input)}
            value={username}
            validators={{
              emptyError: value => !isEmpty(value),
            }}
            label={t('username')}
            onChange={username => this.setState({ username })}
          />
          <Input
            onRef={input => (this.password = input)}
            value={password}
            type="password"
            validators={{
              emptyError: value => !isEmpty(value),
            }}
            label={t('password')}
            onChange={password => this.setState({ password })}
          />
          <Button size="large" className={classes.loginBtn} type="submit">
            {t('login')}
          </Button>
        </form>
        <Typography className={classes.errorText}>{t(errorText)}</Typography>
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  errorText: state.auth.errorText,
});

LogForm.propTypes = {
  errorText: PropTypes.string.isRequired,
};

export default withRouter(
  withNamespaces('AuthScreen')(
    withStyles(styles)(
      connect(
        mapStateToProps,
        { loginUser: actions.loginUser },
      )(LogForm),
    ),
  ),
);
