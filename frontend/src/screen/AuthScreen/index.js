import React from 'react';

import LogForm from './LogForm';
import RegForm from './RegForm';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  authScreen: {
    display: 'flex',
    flexDirection: 'row',
    '& > *': {
      marginTop: '1rem',
      flex: '1',
    },
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
});

const AuthScreen = ({ text, classes }) => (
  <div className={classes.authScreen}>
    <LogForm />
    <RegForm />
  </div>
);

AuthScreen.propTypes = {
  text: PropTypes.string,
};

AuthScreen.defaultProps = {
  text: 'Nie ustawili mi defaultPropsa',
};

export default withStyles(styles)(AuthScreen);
