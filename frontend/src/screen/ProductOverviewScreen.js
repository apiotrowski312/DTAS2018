import * as actions from '../store/product/actions';
import * as actionsUser from '../store/user/actions';
import React from 'react';
import { Redirect } from 'react-router'
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import Product from '../common/Product';

class ProductOverviewScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      category: '',
      condition: '',
      quantity: '',
      postMethod: '',
      price: '',
      type: '',
      active: '',
      publicationDate: '',
      endDate: '',
      postedBy: '',
      boughtBy: [],
      bidQuantity: '1',
      bidPrice: '',
      currentUserId: '',
    };
  }

  componentWillMount() {
    this.getData();
  }

  getData() {
    this.props.getProduct(this.props.match.params.id).then(productData => {
      if (productData !== undefined) {
        this.setState({
          title: productData.title,
          description: productData.description,
          category: productData.category,
          condition: productData.condition,
          quantity: productData.quantity,
          postMethod: productData.postMethod,
          price: productData.price,
          type: productData.type,
          active: productData.active,
          publicationDate: productData.publicationDate,
          postedBy: productData.postedBy,
          boughtBy: productData.boughtBy,
          endDate: productData.endDate,
          bidPrice: productData.price + 1,
          bidQuantity: 1,
          productImages: productData.productImages,
        });
      }
    });

    this.props.getUserData().then(userData => {
      if (userData !== undefined) {
        this.setState({
          currentUserId: userData.id,
        });
      }
    });
  }

  handleProductQuantityChange = name => event => {
    if (event.target.value >= 1 && event.target.value <= this.state.quantity) {
      this.setState({
        [name]: event.target.value,
      });
    }
  };

  handleProductBidChange = name => event => {
    if (event.target.value > this.state.price) {
      this.setState({
        [name]: event.target.value,
      });
    }
  };

  handleProductBuy = event => {
    event.preventDefault();

    this.props.buyProduct(this.props.match.params.id, this.state.bidQuantity).then(response => {
      if (response !== undefined) {
        this.getData();
      }
    });
  }

  handleProductBid = event => {
    event.preventDefault();

    this.props.bidProduct(this.props.match.params.id, this.state.bidPrice).then(response => {
      if (response !== undefined) {
        this.getData();
      }
    });
  }

  render() {
    const { error } = this.props;

    if (error === true) {
      return <Redirect to="/"/>
    }

    return (

      <Product
        title = {this.state.title}
        description = {this.state.description}
        category = {this.state.category}
        condition = {this.state.condition}
        quantity = {this.state.quantity}
        postMethod = {this.state.postMethod}
        price = {this.state.price}
        type = {this.state.type}
        active = {this.state.active}
        publicationDate = {this.state.publicationDate}
        endDate = {this.state.endDate}
        postedBy = {this.state.postedBy}
        boughtBy = {this.state.boughtBy}
        bidQuantity = {this.state.bidQuantity}
        bidPrice = {this.state.bidPrice}
        productImages = {this.state.productImages}
        currentUserId = {this.state.currentUserId}
        handleProductQuantityChange = {this.handleProductQuantityChange}
        handleProductBidChange = {this.handleProductBidChange}
        handleProductBuy = {this.handleProductBuy}
        handleProductBid = {this.handleProductBid}
      />
    );
  }
}

const mapStateToProps = state => ({
  error: state.product.error,
  errorTextUser: state.user.errorText,
});

export default withNamespaces('Product')(
  connect(
    mapStateToProps,
    { getProduct: actions.getProduct,
      buyProduct: actions.buyProduct,
      bidProduct: actions.bidProduct,
      getUserData: actionsUser.getUserData },
  )(ProductOverviewScreen),
);
