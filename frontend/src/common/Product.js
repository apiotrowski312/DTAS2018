import React from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Carousel from 'nuka-carousel';
import ArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeftSharp';
import ArrowRightIcon from '@material-ui/icons/KeyboardArrowRightSharp';
import { withNamespaces } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import config from '../config';

const styles = theme => ({
  product: {
    padding: theme.spacing.unit,
  },

  product__wrapper: {
    padding: theme.spacing.unit,
  },

  product__title: {
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing.unit,
    },
    [theme.breakpoints.up('md')]: {
      marginBottom: 4 * theme.spacing.unit,
    },
  },

  product__properties: {
    listStyle: 'none',
    margin: 0,
    padding: 0,
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',

    [theme.breakpoints.down('sm')]: {
      marginBottom: 2 * theme.spacing.unit,
    },
    [theme.breakpoints.up('md')]: {
      marginBottom: 4 * theme.spacing.unit,
    },
  },

  product__property: {
    marginBottom: '4px',
  },

  product__key: {
    display: 'inline-block',
    width: '120px',
  },

  product__value: {
    display: 'inline-block',
  },

  product__price_field: {
    [theme.breakpoints.down('xs')]: {
      marginTop: 4 * theme.spacing.unit,
    },
    [theme.breakpoints.down('sm')]: {
      marginBottom: 4 * theme.spacing.unit,
      maxWidth: '50%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      justifyContent: 'center',
    },
  },

  product__action_button: {
    display: 'block',
    backgroundColor: theme.palette.primary.main,
    color: '#fff',
    marginTop: '12px',
    width: '100%',
  },

  product__slider: {
    '& ul': {
      height: '200px',
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: '12px',
    },
    [theme.breakpoints.up('md')]: {},
  },

  product__slider_frame: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '12px 0',
  },

  product__slider_img: {
    [theme.breakpoints.down('sm')]: {
      maxWidth: '80%',
      maxHeight: '260px',
    },
    [theme.breakpoints.up('md')]: {
      maxHeight: '360px',
      maxWidth: '80%',
    },
  },

  product__slider_control: {
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    height: '24px',
    width: '24px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  product__slider_arrow: {
    fill: '#fff',
  },
});

class Product extends React.Component {
  constructor(props) {
    super(props);
  }

  convertDate(date) {
    return new Date(date).toISOString().substring(0, 10);
  }

  writeCategories(categories, t) {
    var categoriesString = '';

    for (var value of categories) {
      if (categoriesString === '') categoriesString += t(value);
      else categoriesString += ', ' + t(value);
    }

    return categoriesString;
  }

  countNumberOfItemsUserBought(arr, userId) {
    var number = 0;
    for (var i = 0; i < arr.length; ++i) {
      if (arr[i] == userId) number++;
    }
    return number;
  }

  render() {
    const { t, classes } = this.props;
    var publicationDate,
      endDate,
      priceField = [],
      boughtItems = this.countNumberOfItemsUserBought(
        this.props.boughtBy,
        this.props.currentUserId,
      ),
      images = [];

    if (this.props.publicationDate !== '' && this.props.endDate !== '') {
      publicationDate = this.convertDate(this.props.publicationDate);
      endDate = this.convertDate(this.props.publicationDate);
    }

    priceField.push(<Typography variant="subtitle1">{t('price')}</Typography>);
    priceField.push(<Typography variant="h4">{this.props.price}</Typography>);

    if (this.props.active === true) {
      if (this.props.postedBy !== this.props.currentUserId) {
        if (this.props.type === 'Buy now') {
          priceField.push(
            <TextField
              id="standard-number"
              label={t('quantity')}
              value={this.props.bidQuantity}
              onChange={this.props.handleProductQuantityChange('bidQuantity')}
              type="number"
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />,
          );
          priceField.push(
            <Button
              size="large"
              className={classes.product__action_button}
              onClick={this.props.handleProductBuy}>
              {t('buyNow')}
            </Button>,
          );
        } else if (this.props.type === 'Bid') {
          if (boughtItems === 0) {
            priceField.push(
              <TextField
                id="standard-number"
                label={t('bidValue')}
                value={this.props.bidPrice}
                onChange={this.props.handleProductBidChange('bidPrice')}
                type="number"
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
              />,
            );
            priceField.push(
              <Button
                size="large"
                className={classes.product__action_button}
                onClick={this.props.handleProductBid}>
                {t('doBid')}
              </Button>,
            );
          } else {
            priceField.push(<Typography variant="subtitle1">{t('youAreLeading')}</Typography>);
          }
        }
      } else {
        priceField.push(<Typography variant="subtitle1">{t('auctionIsYours')}</Typography>);
      }
      priceField.push(
        <Typography variant="subtitle1">
          {t('endDate')}: {endDate}
        </Typography>,
      );
    } else {
      priceField.push(<Typography variant="subtitle1">{t('auctionHasEnded')}</Typography>);
    }

    if (boughtItems > 0) {
      if (this.props.type === 'Buy now')
        priceField.push(
          <Typography variant="subtitle1">
            {t('itemsYouBought')}: {boughtItems}
          </Typography>,
        );
      else {
        if (this.props.active === false)
          priceField.push(<Typography variant="subtitle1">{t('youAreWinner')}</Typography>);
      }
    }

    if (this.props.productImages !== null) {
      this.props.productImages.forEach(function(i) {
        images.push(
          <div className={classes.product__slider_frame}>
            <img
              className={classes.product__slider_img}
              src={config.apiUrl + '/' + i}
              alt="image"
            />
          </div>,
        );
      });
    }

    return (
      <div className={classes.product}>
        <Paper className={classes.product__wrapper}>
          <Typography variant={'headline'} className={classes.product__title}>
            {this.props.title}
          </Typography>
          <Grid container>
            <Grid item xs="12" sm="6" md="3">
              <ul className={classes.product__properties}>
                <li className={classes.product__property}>
                  <span className={classes.product__key}>{t('publicationDate')}</span>
                  <span className={classes.product__value}>{publicationDate}</span>
                </li>
                <li className={classes.product__property}>
                  <span className={classes.product__key}>{t('category')}</span>
                  <span className={classes.product__value}>
                    {this.writeCategories(this.props.category, t)}
                  </span>
                </li>
                <li className={classes.product__property}>
                  <span className={classes.product__key}>{t('condition')}</span>
                  <span className={classes.product__value}>
                    {t(this.props.condition.toLowerCase())}
                  </span>
                </li>
                <li className={classes.product__property}>
                  <span className={classes.product__key}>{t('quantity')}</span>
                  <span className={classes.product__value}>{this.props.quantity}</span>
                </li>
              </ul>
              <Typography variant="subtitle1">{t('description')}</Typography>
              <Typography variant="body1">{this.props.description}</Typography>
            </Grid>
            <Grid item xs="12" sm="6" md="3" className={classes.product__price_field}>
              <div>{priceField}</div>
            </Grid>
            <Grid item xs="12" sm="12" md="6">
              <div className={classes.product__slider}>
                {this.props.productImages !== null && (
                  <Carousel
                    renderCenterLeftControls={({ previousSlide }) => (
                      <div className={classes.product__slider_control} onClick={previousSlide}>
                        <ArrowLeftIcon className={classes.product__slider_arrow} />
                      </div>
                    )}
                    renderCenterRightControls={({ nextSlide }) => (
                      <div className={classes.product__slider_control} onClick={nextSlide}>
                        <ArrowRightIcon className={classes.product__slider_arrow} />
                      </div>
                    )}>
                    {images}
                  </Carousel>
                )}
                {this.props.productImages === null && (
                  <Typography variant="subtitle1">{t('noPictures')}</Typography>
                )}
              </div>
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

Product.defaultProps = {
  title: '',
  description: '',
  category: '',
  condition: '',
  quantity: '',
  postMethod: '',
  price: '',
  type: '',
  active: '',
  publicationDate: '',
  endDate: '',
  postedBy: '',
  boughtBy: [],
  bidQuantity: '',
  bidPrice: '',
  currentUserId: '',
  productImages: null,
};

export default withNamespaces('Product')(withStyles(styles)(Product));
