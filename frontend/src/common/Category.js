import { Paper, Typography } from '@material-ui/core/';

import PropTypes from 'prop-types';
import React from 'react';
import logo from '../logo.svg';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  mainStyle: {
    maxWidth: 200,
    maxHeight: 200,
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.secondary.main,
    width: 128,
    height: 128,
  },

  img: {
    margin: 'auto',
    maxWidth: '100%',
    maxHeight: '100%',
  },
});

const Category = ({ picture, category, classes }) => (
  <Paper className={classes.mainStyle}>
    <img className={classes.img} alt="complex" src={picture} />
    <Typography align="center">{category}</Typography>
  </Paper>
);

Category.propTypes = {
  classes: PropTypes.object.isRequired,
  category: PropTypes.string,
};

Category.defaultProps = {
  picture: logo,
  category: 'Kategoria',
};

export default withStyles(styles)(Category);
