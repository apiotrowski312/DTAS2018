import { Paper, Typography } from '@material-ui/core/';

import PropTypes from 'prop-types';
import React from 'react';
import logo from '../../logo.svg';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  mainStyle: {
    flexGrow: 1,
    width: 400,
    height: 400,
    backgroundColor: theme.palette.secondary.main,
    alignItems: 'center',
    justifyItems: 'center',
  },
  image: {
    display: 'flex',
    position: 'relative',
    height: 280,
    width: 400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    display: 'block',
    maxWidth: 280,
    maxHeight: 280,
  },
  typo: {
    marginRight: '10px',
    marginBottom: '5px',
  },
});

const ProductPromoSquare = ({ price, title, picture, classes }) => (
  <Paper className={classes.mainStyle}>
    <div className={classes.image}>
      <img className={classes.img} alt="complex" src={picture} />
    </div>
    <Typography align="center">{title}</Typography>
    <Typography className={classes.typo} variant="h5" align="right">
      {price}
    </Typography>
  </Paper>
);

ProductPromoSquare.propTypes = {
  classes: PropTypes.object.isRequired,
  price: PropTypes.string,
  title: PropTypes.string,
};

ProductPromoSquare.defaultProps = {
  picture: logo,
  price: 'Cena',
  title: 'Proponowany produkt',
};

export default withStyles(styles)(ProductPromoSquare);
