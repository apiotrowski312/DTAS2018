import { Paper, Typography } from '@material-ui/core/';

import PropTypes from 'prop-types';
import React from 'react';
import logo from '../../logo.svg';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  mainStyle: {
    flexGrow: 1,
    maxWidth: 500,
    maxHeight: 200,
    backgroundColor: theme.palette.secondary.main,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    display: 'flex',
    position: 'relative',
  },
  img: {
    margin: '5px auto 5px auto',
    display: 'block',
    maxWidth: 350,
    maxHeight: 125,
  },
  typo: {
    marginRight: '10px',
    marginBottom: '5px',
  },
});

const ProductPromoLarge = ({ price, title, picture, classes }) => (
  <Paper className={classes.mainStyle}>
    <div className={classes.image}>
      <img className={classes.img} alt="complex" src={picture} />
    </div>
    <Typography align="center">{title}</Typography>
    <Typography className={classes.typo} variant="h5" align="right">
      {price}
    </Typography>
  </Paper>
);

ProductPromoLarge.propTypes = {
  classes: PropTypes.object.isRequired,
  price: PropTypes.string,
  title: PropTypes.string,
};

ProductPromoLarge.defaultProps = {
  picture: logo,
  price: 'Cena',
  title: 'Proponowany produkt',
};

export default withStyles(styles)(ProductPromoLarge);
