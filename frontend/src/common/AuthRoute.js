import { Redirect, Route } from 'react-router-dom';

import React from 'react';
import SETTING from 'config';

const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem(SETTING.cacheTokenKey) ? (
        <Component />
      ) : (
        <Redirect
          to={{
            pathname: SETTING.path.start,
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

export default AuthRoute;
