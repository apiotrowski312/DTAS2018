import {
  AppBar,
  Button,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Typography,
  InputBase,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React, { PureComponent } from 'react';
import { fade } from '@material-ui/core/styles/colorManipulator';
import MoreIcon from '@material-ui/icons/MoreVert';
import SETTINGS from '../config';
import Toolbar from '@material-ui/core/Toolbar';
import { withNamespaces } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { LOGOUT } from '../store/auth/definitions';
const styles = theme => ({
  button: {
    fontSize: '1.2rem',
    color: 'white',
  },
  img: {
    marginRight: 'auto',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export class TopBar extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isMobileMenuOpen: false,
      anchorEl: null,
    };
  }

  handleMenu = event => {
    this.setState({
      anchorEl: event.currentTarget,
      isMobileMenuOpen: true,
    });
  };

  handleClose = () => {
    this.setState({ anchorEl: null, isMobileMenuOpen: false });
  };

  buttonArray = () => {
    const { t, history, i18n } = this.props;
    return [
      {
        onClick: () => {
          history.push(`${SETTINGS.path.landingPage}`);
        },
        label: t('Home'),
      },
      {
        onClick: () => {
          history.push(`${SETTINGS.path.addProd}`);
        },
        label: t('Add Product'),
      },
      {
        onClick: () => {
          localStorage.getItem(SETTINGS.cacheTokenKey) === null
            ? history.push(`${SETTINGS.path.auth}`)
            : history.push(`${SETTINGS.path.user}`);
        },
        label: localStorage.getItem(SETTINGS.cacheTokenKey) === null ? t('Login') : t('User'),
      },
      {
        visible: localStorage.getItem(SETTINGS.cacheTokenKey) != null,
        onClick: () => {
          this.props.dispatch({ type: LOGOUT });
          history.push(`${SETTINGS.path.landingPage}`);
        },
        label: t('Logout'),
      },
      {
        onClick: () => {
          console.log(localStorage.getItem('i18nextLng'));
          if (localStorage.getItem('i18nextLng') == 'pl') {
            i18n.changeLanguage('en');
          } else {
            i18n.changeLanguage('pl');
          }
        },
        label: t(localStorage.getItem('i18nextLng') != 'pl' ? 'EN' : 'PL'),
      },
    ];
  };

  renderView = () => {
    const { classes } = this.props;
    return (
      <Grid
        className={classes.sectionDesktop}
        container
        direction="row"
        justify="flex-end"
        alignItems="center"
        spacing={8}>
        {this.buttonArray().map(row => (
          <Grid key={row.label} item>
            {row.visible !== false && (
              <Button variant="text" onClick={row.onClick} className={classes.button}>
                {row.label}
              </Button>
            )}
          </Grid>
        ))}
      </Grid>
    );
  };

  renderMobileView = () => {
    const { classes } = this.props;
    const { isMobileMenuOpen, anchorEl } = this.state;

    return (
      <Menu
        open={isMobileMenuOpen}
        onClose={this.handleClose}
        anchorEl={anchorEl}
        className={classes.sectionMobile}>
        {this.buttonArray().map(row => (
          <MenuItem key={row.label}>
            <Button onClick={row.onClick}>{row.label}</Button>
          </MenuItem>
        ))}
      </Menu>
    );
  };

  render() {
    const { t, classes } = this.props;
    return (
      <AppBar position="sticky">
        <Toolbar style={{ justifyContent: 'space-beetwen' }}>
          <Typography className={classes.title} variant="h5" color="inherit">
            Alledrogo
          </Typography>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
            />
          </div>
          <IconButton onClick={event => this.handleMenu(event)} className={classes.sectionMobile}>
            <MoreIcon />
          </IconButton>
          {this.renderView()}
          {this.renderMobileView()}
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(styles)(withNamespaces('Navigation')(withRouter(connect()(TopBar))));
