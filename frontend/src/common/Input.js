import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';
import { omit } from 'lodash';
import { withNamespaces } from 'react-i18next';

export class Input extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isValid: true,
      touched: false,
      errorText: '',
      value: props.value || '',
      focused: false,
    };

    if (props.onRef) {
      props.onRef(this);
    }
  }

  validator = value => {
    const { validators, t } = this.props;
    let errorText = '';

    Object.keys(validators).every(key => {
      console.log(value, '!!!!!!!!!!!!!!!!!!!!!!!');
      if (!validators[key](value)) {
        errorText = t(key);
        return false;
      }
      return true;
    });
    return { errorText, isValid: errorText === '' };
  };

  isValid = () => {
    const { value } = this.props;
    const text = this.validator(value);
    this.setState({ ...text, touched: true });

    return text.isValid;
  };

  clear = () => {
    this.setState({ value: '', isValid: true, touched: false, focused: false });
  };

  handleLostFocus = () => this.isValid();

  handleOnChange(value) {
    const { onChange } = this.props;
    const text = this.validator(value);

    if (typeof onChange === 'function') {
      this.setState({ ...text, value });
      onChange(value);
    }
  }

  render() {
    const { variant, onChange, type, helperText, ...restProps } = this.props;
    const { value, isValid, errorText } = this.state;

    const restOfValidProps = omit(restProps, [
      't',
      'tReady',
      'i18n',
      'reportNS',
      'lng',
      'i18nOptions',
      'defaultNS',
      'onRef',
    ]);

    return (
      <TextField
        type={type}
        variant={variant}
        onChange={event => this.handleOnChange(event.target.value)}
        value={value}
        onBlur={this.handleLostFocus}
        error={!isValid}
        helperText={isValid ? helperText : errorText}
        {...restOfValidProps}
      />
    );
  }
}

Input.propTypes = {
  onChange: PropTypes.func,
  variant: PropTypes.string,
  type: PropTypes.string,
  validators: PropTypes.object,
  helperText: PropTypes.string,
};

Input.defaultProps = {
  onChange: () => null,
  variant: 'outlined',
  type: 'text',
  validators: {},
  helperText: ' ',
};

export default withNamespaces('validator')(Input);
