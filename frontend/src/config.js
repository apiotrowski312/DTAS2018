const config: configType = {
  path: {
    start: '/',
    user: '/user',
    basket: '/basket',
    landingPage: '/app',
    auth: '/auth',
    addProd: '/addProd',
    getProduct: '/product/:id',
    buyProduct: '/product/buy/:id',
    bidProduct: '/product/bid/:id'
  },
  apiUrl: process.env.REACT_APP_API_URL || 'http://localhost:8000',

  url: {
    login: '/users/authenticate',
    register: '/users/register',
    getCurrentUser: '/users/current',
    users: '/users',
    addProd: '/products/createOffer',
    getProduct: '/products',
    buyProduct: '/products/buy',
    bidProduct: '/products/bid'
  },
  cacheTokenKey: 'token',
};

export default config;
