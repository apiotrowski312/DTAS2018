import { en, pl } from './translations';

import LanguageDetector from 'i18next-browser-languagedetector';
import i18n from 'i18next';

i18n.use(LanguageDetector).init({
  // we init with resources
  resources: {
    en,
    pl,
  },

  fallbackLng: 'en',
  debug: process.env.NODE_ENV === 'development',

  // have a common namespace used around the full app
  ns: ['translations'],
  defaultNS: 'translations',

  keySeparator: false, // we use content as keys

  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: ',',
  },

  react: {
    wait: true,
  },
});

export default i18n;
