import {
  GET_USER_FULFILLED,
  GET_USER_PENDING,
  GET_USER_REJECTED,
  PUT_USER_FULFILLED,
  PUT_USER_PENDING,
  PUT_USER_REJECTED,
} from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';

export const getUserData = () => {
  return dispatch => {
    dispatch({ type: GET_USER_PENDING });
    return axios
      .get(`${SETTINGS.apiUrl}${SETTINGS.url.getCurrentUser}`, {
        headers: { Authorization: `Bearer ${localStorage.getItem(SETTINGS.cacheTokenKey)}` },
      })
      .then(response => {
        dispatch({ type: GET_USER_FULFILLED, payload: response.data });
        return response.data;
      })
      .catch(err => {
        console.log(err);
        if (err.response === undefined) {
          dispatch({ type: GET_USER_REJECTED, payload: { errorText: 'networkError' } });
        } else if (err.response.status === 401) {
          dispatch({ type: GET_USER_REJECTED, payload: { errorText: 'loginError' } });
        } else {
          dispatch({ type: GET_USER_REJECTED, payload: { errorText: 'unknownError' } });
        }
      });
  };
};

export const updateUser = (id, updateForm) => {
  return dispatch => {
    dispatch({ type: PUT_USER_PENDING });
    return axios
      .put(`${SETTINGS.apiUrl}${SETTINGS.url.users}/${id}`, updateForm, {
        headers: { Authorization: `Bearer ${localStorage.getItem(SETTINGS.cacheTokenKey)}` },
      })
      .then(response => {
        dispatch({ type: PUT_USER_FULFILLED });
        return true;
      })
      .catch(err => {
        console.log(err);
        if (err.response === undefined) {
          dispatch({ type: PUT_USER_REJECTED, payload: { errorText: 'userError' } });
        } else if (err.response.status === 401) {
          dispatch({ type: PUT_USER_REJECTED, payload: { errorText: 'updateError' } });
        } else if (err.response.status === 400) {
          dispatch({
            type: PUT_USER_REJECTED,
            payload: { errorText: err.response.data.message },
          });
        } else {
          dispatch({ type: PUT_USER_REJECTED, payload: { errorText: 'unknownError' } });
        }
      });
  };
};
