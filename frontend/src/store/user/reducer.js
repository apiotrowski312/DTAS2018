import {
  GET_USER_FULFILLED,
  GET_USER_PENDING,
  GET_USER_REJECTED,
  PUT_USER_FULFILLED,
  PUT_USER_PENDING,
  PUT_USER_REJECTED,
} from './definitions';

const defaultState = {
  userData: {},
  errorText: ' ',
};

const user = function(state: Auth = defaultState, action: Action<Auth>) {
  switch (action.type) {
    case GET_USER_PENDING:
    case PUT_USER_PENDING:
      return { ...state, pending: true };
    case GET_USER_FULFILLED:
      return { ...state, userData: action.payload };
    case PUT_USER_REJECTED:
    case GET_USER_REJECTED:
      return { ...state, pending: false, errorText: action.payload.errorText };
    case PUT_USER_FULFILLED:
    default:
      return state;
  }
};

export default user;
