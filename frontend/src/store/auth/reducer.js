import {
  LOGIN_FULFILLED,
  LOGIN_PENDING,
  LOGIN_REJECTED,
  REGISTER_FULFILLED,
  REGISTER_PENDING,
  REGISTER_REJECTED,
  LOGOUT,
} from './definitions';

import SETTINGS from '../../config';

const defaultState = {
  pending: false,
  errorText: ' ',
  token: localStorage.getItem(SETTINGS.cacheTokenKey) || '',
};

const auth = function(state: Auth = defaultState, action: Action<Auth>) {
  switch (action.type) {
    case REGISTER_PENDING:
    case LOGIN_PENDING:
      return { ...state, pending: true };
    case REGISTER_FULFILLED:
      return { ...defaultState };
    case LOGIN_FULFILLED: {
      const { token } = action.payload;
      localStorage.setItem(SETTINGS.cacheTokenKey, token);
      return { ...defaultState, token };
    }
    case REGISTER_REJECTED:
    case LOGIN_REJECTED:
      return { ...state, pending: false, errorText: action.payload.errorText, token: '' };
    case LOGOUT:
      localStorage.removeItem(SETTINGS.cacheTokenKey);
      return defaultState;
    default:
      return state;
  }
};

export default auth;
