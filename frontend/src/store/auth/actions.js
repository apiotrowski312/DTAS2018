import {
  LOGIN_FULFILLED,
  LOGIN_PENDING,
  LOGIN_REJECTED,
  REGISTER_FULFILLED,
  REGISTER_PENDING,
  REGISTER_REJECTED,
} from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';

export const loginUser = loginForm => {
  return dispatch => {
    dispatch({ type: LOGIN_PENDING });
    return axios
      .post(`${SETTINGS.apiUrl}${SETTINGS.url.login}`, loginForm)
      .then(response => {
        dispatch({ type: LOGIN_FULFILLED, payload: { token: response.data.token } });
        return response.data.token;
      })
      .catch(err => {
        console.log(err);
        if (err.response === undefined) {
          dispatch({ type: LOGIN_REJECTED, payload: { errorText: 'networkError' } });
        } else if (err.response.status === 401) {
          dispatch({ type: LOGIN_REJECTED, payload: { errorText: 'loginError' } });
        } else {
          dispatch({ type: LOGIN_REJECTED, payload: { errorText: 'unknownError' } });
        }
      });
  };
};

export const registerUser = registerForm => {
  return dispatch => {
    dispatch({ type: REGISTER_PENDING });
    return axios
      .post(`${SETTINGS.apiUrl}${SETTINGS.url.register}`, registerForm)
      .then(response => {
        dispatch({ type: REGISTER_FULFILLED });
        return true;
      })
      .catch(err => {
        console.log(err);
        if (err.response === undefined) {
          dispatch({ type: REGISTER_REJECTED, payload: { errorText: 'networkError' } });
        } else if (err.response.status === 401) {
          dispatch({ type: REGISTER_REJECTED, payload: { errorText: 'registerError' } });
        } else if (err.response.status === 400) {
          dispatch({
            type: REGISTER_REJECTED,
            payload: { errorText: err.response.data.message },
          });
        } else {
          dispatch({ type: REGISTER_REJECTED, payload: { errorText: 'unknownError' } });
        }
      });
  };
};
