import { ADD_FULFILLED, ADD_PENDING, ADD_REJECTED } from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';

const FormData = require('form-data');

export const addProduct = addProductForm => {
  const form = new FormData();

  form.append('title', addProductForm.title);

  addProductForm.productImages.forEach((image, index) => {
    form.append(`productImages`, image, image.name);
  });
  form.append('description', addProductForm.description);

  addProductForm.category.forEach((cat, index) => {
    form.append(`category[${index}]`, cat);
  });

  addProductForm.postMethod.forEach((method, index) => {
    form.append(`postMethod[${index}][0]`, method[0]);
    form.append(`postMethod[${index}][1]`, method[1]);
  });

  form.append('condition', addProductForm.condition);
  form.append('quantity', addProductForm.quantity);
  form.append('type', addProductForm.type);
  form.append('price', addProductForm.price);
  form.append('endDate', addProductForm.endDate);

  return dispatch => {
    dispatch({ type: ADD_PENDING });
    return axios
      .post(`${SETTINGS.apiUrl}${SETTINGS.url.addProd}`, form, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem(SETTINGS.cacheTokenKey)}`,
        },
      })
      .then(response => {
        dispatch({ type: ADD_FULFILLED });
        return true;
      })
      .catch(err => {
        console.log(err);
        if (err.response === undefined) {
          dispatch({ type: ADD_REJECTED, payload: { errorText: 'networkError' } });
        } else if (err.response.status === 401) {
          dispatch({ type: ADD_REJECTED, payload: { errorText: 'addError' } });
        } else if (err.response.status === 400) {
          dispatch({
            type: ADD_REJECTED,
            payload: { errorText: err.response.data.message },
          });
        } else {
          dispatch({ type: ADD_REJECTED, payload: { errorText: 'unknownError' } });
        }
      });
  };
};
