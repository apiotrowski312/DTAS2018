import { ADD_FULFILLED, ADD_PENDING, ADD_REJECTED } from './definitions';

import SETTINGS from '../../config';

const defaultState = {
  pending: false,
  errorText: ' ',
  token: localStorage.getItem(SETTINGS.cacheTokenKey) || '',
};

const prod = function i18nState(state = defaultState, action) {
  switch (action.type) {
    case ADD_PENDING:
      return { ...state, pending: true };
    case ADD_FULFILLED: {
      //const { token } = action.payload;
      //localStorage.setItem(SETTINGS.cacheTokenKey, token);
      return { ...defaultState };
    }
    case ADD_REJECTED:
      return { ...state, pending: false, errorText: action.payload.errorText, token: '' };
    default:
      return state;
  }
};

export default prod;
