import { combineReducers } from 'redux';
import i18n from './i18n/reducer';
import auth from './auth/reducer';
import user from './user/reducer';
import product from './product/reducer';

export default combineReducers({
  i18n,
  auth,
  user,
  product,
});
