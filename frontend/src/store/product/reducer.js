import { 
  GET_SINGLE_FULFILLED,
  GET_SINGLE_PENDING,
  GET_SINGLE_REJECTED,
  BUY_FULFILLED,
  BUY_PENDING,
  BUY_REJECTED,
  BID_FULFILLED,
  BID_PENDING,
  BID_REJECTED,
} from './definitions';

import SETTINGS from '../../config';

const defaultState = {
  pending: false,
  errorText: ' ',
  token: localStorage.getItem(SETTINGS.cacheTokenKey) || '',
  productData: {},
};

const prod = function i18nState(state = defaultState, action) {
  switch (action.type) {
    case GET_SINGLE_PENDING:
      return { ...state, pending: true, error: false };
    case GET_SINGLE_FULFILLED:
      return { ...state, productData: action.payload, error: false };
    case GET_SINGLE_REJECTED:
      return { ...state, pending: false, error: true, token: '' };
    case BUY_PENDING:
      return { ...state, pending: true, error: false };
    case BUY_FULFILLED:
      return { ...state, productData: action.payload, error: false };
    case BUY_REJECTED:
      return { ...state, pending: false, error: true, token: '' };
    case BID_PENDING:
      return { ...state, pending: true, error: false };
    case BID_FULFILLED:
      return { ...state, productData: action.payload, error: false };
    case BID_REJECTED:
      return { ...state, pending: false, error: true, token: '' };
    default:
      return state;
  }
};

export default prod;
