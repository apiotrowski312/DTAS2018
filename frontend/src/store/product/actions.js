import { 
  GET_SINGLE_FULFILLED,
  GET_SINGLE_PENDING,
  GET_SINGLE_REJECTED,
  BUY_FULFILLED,
  BUY_PENDING,
  BUY_REJECTED,
  BID_FULFILLED,
  BID_PENDING,
  BID_REJECTED,
  } from './definitions';

import SETTINGS from '../../config';
import axios from 'axios';

export const getProduct = id => {
  return dispatch => {
    dispatch({ type: GET_SINGLE_PENDING });
    return axios
      .get(`${SETTINGS.apiUrl}${SETTINGS.url.getProduct}/${id}`, {
        headers: { Authorization: `Bearer ${localStorage.getItem(SETTINGS.cacheTokenKey)}` },
      })
      .then(response => {
        dispatch({ type: GET_SINGLE_FULFILLED, payload: response.data });
        return response.data;
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: GET_SINGLE_REJECTED });
      });
  };
};

export const buyProduct = (id, quantity) => {
  return dispatch => {
    dispatch({ type: BUY_PENDING });
    return axios
      .put(`${SETTINGS.apiUrl}${SETTINGS.url.buyProduct}/${id}`, { quantity: quantity }, {
        headers: { Authorization: `Bearer ${localStorage.getItem(SETTINGS.cacheTokenKey)}` },
      })
      .then(response => {
        dispatch({ type: BUY_FULFILLED });
        return true;
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: BUY_REJECTED });
      });
  };
};

export const bidProduct = (id, price) => {
  return dispatch => {
    dispatch({ type: BID_PENDING });
    return axios
      .put(`${SETTINGS.apiUrl}${SETTINGS.url.bidProduct}/${id}`, { price: price }, {
        headers: { Authorization: `Bearer ${localStorage.getItem(SETTINGS.cacheTokenKey)}` },
      })
      .then(response => {
        dispatch({ type: BID_FULFILLED });
        return true;
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: BID_REJECTED });
      });
  };
};

export const getProducts = id => {
  return dispatch => {
    dispatch({ type: GET_SINGLE_PENDING });
    return axios
      .get(`${SETTINGS.apiUrl}${SETTINGS.url.getProduct}/`, {
        headers: { Authorization: `Bearer ${localStorage.getItem(SETTINGS.cacheTokenKey)}` },
      })
      .then(response => {
        dispatch({ type: GET_SINGLE_FULFILLED, payload: response.data });
        return response.data;
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: GET_SINGLE_REJECTED });
      });
  };
};